<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main class for plugin 'media_pod'
 *
 * @package   media_pod
 * @copyright 2023 bertrand chartier
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Player that creates pod embedding based on media_youtube.
 *
 * @package   media_pod
 * @author    2011 The Open University
 * @copyright 2023 bertrand chartier
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class media_pod_plugin extends core_media_player_external {
    /**
     * Stores whether the playlist regex was matched last time when
     * {@see list_supported_urls()} was called
     * @var bool
     */
    protected $isplaylist = false;

    /**
     * Stores whether the draft regex was matched last time when
     * {@see list_supported_urls()} was called
     * @var bool
     */ 
    protected $isdraft = false;

    /**
     * Stores the id of the draft version
     * {@see list_supported_urls()} was called
     * @var string
     */ 
    protected $draft;

    /**
     * Stores whether the parameter regex was matched last time when
     * {@see list_supported_urls()} was called
     * @var bool
     */
    protected $hasparameter = false;

    /**
     * Stores the port of the string contain parameter to apply
     * {@see list_supported_urls()} was called
     * @var string
     */
    protected $parameter;

    /**
     * Returns url of a matching peertube video
     * @param array $urls to catch
     * @param array $options
     * @return array PHP array
     */
    public function list_supported_urls(array $urls, array $options = array()) {
        // These only work with a SINGLE url (there is no fallback).
        if (count($urls) == 1) {
            $url = reset($urls);

            // Check against regex.
            if (preg_match($this->get_regex(), $url->out(false), $this->matches)) {
                $hasparameter = false;
                $isdraft = false;
                if ($this->matches[4]!='edit'){                    
                    if ($this->matches[3]=='/video/') {
                        $this->isplaylist = false;
                    }

                    if ($this->matches[3]=='/playlist/') {
                        $this->isplaylist = true;
                    }

                    if (preg_match($this->get_param(), $url->out(false), $this->parameter)) {
                        $this->hasparameter = true;
                    }

                    if (preg_match($this->get_draft(), $url->out(false), $this->draft)) {
                        if($this->draft[0]!=$this->matches[4]) {
                            $this->isdraft = true;
                        }
                        
                    }

                    return array($url);
                }
            }
        }

        return array();
    }

    /**
     * Returns string of html substitution
     * @param moodle_url $url URL of video to be embedded.
     * @param string $name string attribute.
     * @param int $width string attribute: parameter plateform of width's video.
     * @param int $height string attribute: parameter plateform of height's video.
     * @param string $options string
     * @return array PHP string
     */
    protected function embed_external(moodle_url $url, $name, $width, $height, $options) {

        $info = trim($name);
        if (empty($info) or strpos($info, 'http') === 0) {
            $info = get_string('pluginname', 'media_pod');
        }
        $info = s($info);

        self::pick_video_size($width, $height);

        $site=$this->matches[2];
        $endurl=$this->matches[4];

        if ($this->isdraft) {
            $endurl=$endurl.'/'.$this->draft[0].'/?is_iframe=true';
            }

        else {
            if ($this->hasparameter) {
            $endurl=$endurl.$this->parameter[0].'&is_iframe=true';
            }
            else{
                $endurl=$endurl.'?is_iframe=true';

            }
        }

        
        if ($this->isplaylist) {
            return <<<OET
<span class="mediaplugin mediaplugin_pod $bch">
<iframe width="$width" height="$height" src="https://$site/playlist/$endurl" frameborder="0" allowfullscreen="1"></iframe>
</span>
OET;
        } else {
            return <<<OET
<span class="mediaplugin mediaplugin_pod $bch">
<iframe title="$info" width="$width" height="$height"
src="https://$site/video/$endurl" frameborder="0" allowfullscreen="1"></iframe>
</span>
OET;
        }

    }

    /**
     * Returns regular expression used to match URLs for single pod video
     * @return string PHP regular expression e.g. '~^https?://example.org/~'
     */
    protected function get_regex() {
        $urlsplatforms = explode(PHP_EOL, get_config('media_pod', 'url_plateforms'));
        $urlsstring= implode('|',$urlsplatforms);
        // Initial part of link.
        $start = '~^https?:\/\/(www\.|)('.$urlsstring.')(/video/|/playlist/)';
        // Middle bit: Video name value.
        $middle = '([0-9a-z\-\_]+)';

        return $start . $middle . core_media_player_external::END_LINK_REGEX_PART;
    }

    
    /**
     * Returns regular expression used to match draft mode of single pod video
     * @return string PHP regular expression
     */
    protected function get_draft() {
        // to catch video in draft mode.
        $draft='~([0-9A-z]{64})';

        return $draft . core_media_player_external::END_LINK_REGEX_PART;
    }

    
    /**
     * Returns regular expression used to match parameter of single peertube video
     * @return string PHP regular expression e.g. '~^?start=1m2s~'
     */
    protected function get_param() {
        // Parameter of video or playlist.
        $param='~(\?[0-9A-z\=\&]+)';

        return $param . core_media_player_external::END_LINK_REGEX_PART;
    }

    /**
     * Returns array of platform used in this plug-in
     * @return string PHP array
     */
    public function get_embeddable_markers() {
        $urlsplatforms = explode(PHP_EOL, get_config('media_pod', 'url_plateforms'));
        return $urlsplatforms;
    }

    /**
     * Default rank
     * @return int
     */
    public function get_rank() {
        return 1001;
    }
}
