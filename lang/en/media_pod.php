<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for plugin 'media_pod'
 *
 * @package   media_pod
 * @copyright 2023
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Pod';
$string['pluginname_help'] = 'The video-sharing website Pod Video and playlist links are supported.';
$string['privacy:metadata'] = 'The Pod media plugin does not store any personal data.';
$string['supportsvideo'] = 'Pod videos';
$string['supportsplaylist'] = 'Pod playlists';
$string['url_plateforms']=' URL of Pod platforms';
$string['url_plateforms_desc']=' exemple: podeduc.apps.education.fr';
